<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

// пройти тест symfony php bin/phpunit tests/Controller/ConferenceControllerTest.php
class ConferenceControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        // Проверяем, что на странице есть заголовки конференций
        $this->assertGreaterThan(0, $crawler->filter('h4')->count(), 'Conference titles should be displayed');
    }

    //пройти тест
    // php bin/phpunit --filter testConferencePage
    public function testConferencePage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertCount(2, $crawler->filter('h4'));

        $client->clickLink('View');

        $this->assertPageTitleContains('Amsterdam');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Amsterdam 2019');
        $this->assertSelectorExists('div:contains("There are 1 comments")');
    }


    // public function testCommentSubmission()
    // {
    //     $client = static::createClient();
    //     $client->request('GET', '/conference/amsterdam-2019');
    //     $client->submitForm('Submit', [
    //         'comment_form[author]' => 'Fabien',
    //         'comment_form[text]' => 'Some feedback from an automated functional test',
    //         'comment_form[email]' => 'me@automat.ed',
    //         'comment_form[photo]' => dirname(__DIR__, 2) . '/public/test_images/file_ok.png',
    //     ]);
    //     $this->assertResponseRedirects();
    //     $client->followRedirect();
    //     $this->assertSelectorExists('div:contains("There are 2 comments")');
    // }



    // Тест страницу без конференций (пока не заполненные фикстуры)
    // public function testIndexWithEmptyDatabase()
    // {
    //     $client = static::createClient();
    //     $crawler = $client->request('GET', '/');
    //     $this->assertResponseIsSuccessful();
    //     $this->assertEquals(0, $crawler->filter('h4')->count(), 'No conference titles should be displayed');
    //     $this->assertSelectorTextContains('.example-wrapper', 'No conferences available.');
    // }

}
